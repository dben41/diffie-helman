package cs5490;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * This server is also known as Bob
 * @author Daryl
 *
 */
public class Server {

	public static void main(String[] args) {
		ServerSocket MyService;
		Socket serviceSocket = null;
		DataInputStream input;
		DataOutputStream output = null;
	    try {
	    	//start server and get input
	    	MyService = new ServerSocket(2115);
	        System.out.println("Server: Begin program. Assume that g = 19070 and p = 784313.");
	    	serviceSocket = MyService.accept();
	    	input = new DataInputStream(serviceSocket.getInputStream());
	    	output = new DataOutputStream(serviceSocket.getOutputStream());
	    	
	    	//find the calculation
	    	int privateKey = 12007;
	    	int calculatedKey = expo(19070, privateKey, 78313);
	        System.out.println("Server: Sending Bob's computed key. " + calculatedKey);
	    		        
	    	//read message
	        int aliceKey = input.readInt();
	    	System.out.println("Server: Receiving Alice's Computed Key: " + aliceKey);
	    	
	    	//receive Alice's calculated number
	        output.writeInt(calculatedKey);
	    	
           //verify that numbers match
           int finalKey = expo(aliceKey, privateKey, 78313);
           System.out.println("Server: Verifying that Bob's and Alice's key match: " + finalKey );
	        
	    	//close resources
	        input.close();
	        serviceSocket.close();
	        MyService.close();
	        }
	        catch (IOException e) {
	           System.out.println(e);
	        }

	}
	
	
	
	public static int expo(int generator, int exponent, int primeModulus) {
		double  a = generator;
		int totalPower = 1;
		String binaryDigits = Integer.toBinaryString(exponent); //find a binary representation of the private number exponent
		boolean first = true;
		for(char binaryDigit : binaryDigits.toCharArray()){
			if(first)first = false;
			else if(binaryDigit=='0'){
				a = (a * a) % primeModulus;
				totalPower = totalPower * 2;
			}else if(binaryDigit=='1'){
				a = (a * a) % primeModulus;
				a = (a * generator) % primeModulus;
				totalPower = (totalPower * 2) + 1;
			}
		}
		return (int)a;
	}

}
