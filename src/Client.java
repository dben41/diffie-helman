

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;

/**
 * This client is also known as Alice
 * @author Daryl
 *
 */
public class Client {

	public static void main(String[] args) {
		Socket MyClient = null;
		DataOutputStream output = null;
		DataInputStream input;
		try {
	           MyClient = new Socket("localhost", 2115);
	           System.out.println("Client: Begin program. Assume that g = 19070 and p = 784313.");
	           output = new DataOutputStream(MyClient.getOutputStream());
	           input = new DataInputStream(MyClient.getInputStream());
	          	           
	           //find the calculation
	           int privateKey = 160009;
	           int calculatedKey = expo(19070, privateKey, 78313);
	           System.out.println("Client: Sending Alice's computed key. " + calculatedKey);
	           
	           //send the message
	           output.writeInt(calculatedKey);
	           
	           //receive Bob's calculated number
		       int bobKey = input.readInt();
	           System.out.println("Client: Receiving Bob's calculated number: " + bobKey);
	          
	           //verify that numbers match
	           int finalKey = expo(bobKey, privateKey, 78313);
	           System.out.println("Client: Verifying that Bob's and Alice's key match: " + finalKey );
	           
	           //clean up resources
	           MyClient.close();
	           output.close();
	    }catch (IOException e) {
	        System.out.println(e);
	    } 
	}
	

	public static int expo(int generator, int exponent, int primeModulus) {
		double  a = generator;
		int totalPower = 1;
		String binaryDigits = Integer.toBinaryString(exponent); //find a binary representation of the private number exponent
		boolean first = true;
		for(char binaryDigit : binaryDigits.toCharArray()){
			if(first)first = false;
			else if(binaryDigit=='0'){
				a = (a * a) % primeModulus;
				totalPower = totalPower * 2;
			}else if(binaryDigit=='1'){
				a = (a * a) % primeModulus;
				a = (a * generator) % primeModulus;
				totalPower = (totalPower * 2) + 1;
			}
		}
		return (int)a;
	}


}
