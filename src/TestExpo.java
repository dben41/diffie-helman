

public class TestExpo {

	public static void main(String[] args) {
		int answer = diffieHelman(123, 54, 678);

	}
	
	public static int diffieHelman(int generator, int exponent, int primeModulus) {
		double  a = generator;
		int totalPower = 1;
		String binaryDigits = Integer.toBinaryString(exponent); //find a binary representation of the private number exponent
		boolean first = true;
		for(char binaryDigit : binaryDigits.toCharArray()){
			if(first)first = false;
			else if(binaryDigit=='0'){
				a = (a * a) % primeModulus;
				totalPower = totalPower * 2;
			}else if(binaryDigit=='1'){
				a = (a * a) % primeModulus;
				a = (a * generator) % primeModulus;
				totalPower = (totalPower * 2) + 1;
			}
		}
		return (int)a;
	}

}
